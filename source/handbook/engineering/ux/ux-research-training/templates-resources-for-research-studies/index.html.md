---
layout: handbook-page-toc
title: "Templates and resources for research studies"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Templates and resources for research studies

#### User Interview Note Taking
[Note Taking Template](https://docs.google.com/spreadsheets/d/1hnIqg-fnCYW2XKHR8RBsO3cYLSMEZy2xUKmbiUluAY0/edit#gid=0). 

Used to collect raw observations on the participant’s behavior and emotional feedback to the questions, as well as take notes. You can also create a new file directly from the template on Google Drive, by selecting it from one the default styles.

#### Usability Testing Script
[Script Template](https://docs.google.com/document/d/1_5Qu2JR9QE5LE6cK4eq9yJs-nXv2rlWWifcjacaiWdI/edit?usp=sharing).

#### Usability Testing Rainbow Analysis Chart
[Rainbow Analysis Chart](https://docs.google.com/spreadsheets/d/1bPg6op9Sk46lFVGaET-fruE0qz-ctNQsxbZKF-5lpn4/edit#gid=0). 

This approach uses a templated and color-coded spreadsheet to record what participants did during the test. For a thorough walkthrough on how to use this method, check out [this article](https://userresearch.blog.gov.uk/2019/09/13/how-a-spreadsheet-can-make-usability-analysis-faster-and-easier/), or watch [this video of a GitLab researcher's experience using this method](https://drive.google.com/file/d/1fYRTmaHZjMwDQfAnVpaEqHP1dByy1X5x/view?usp=sharing) (starts at 7:00).

#### Shared Google Drive for storing research videos and artifacts
[Shared Google Drive](https://drive.google.com/drive/folders/0AH_zdtW5aioNUk9PVA) - Internal access only.

#### Checklists

The following are examples of checklists that you may want to add to a research issue in order to keep track of what stage the research is up to.

##### User Interviews
```
* [ ] Product Manager: Draft the discussion guide.
* [ ] UX Researcher: Create the screening survey in Qualtrics.
* [ ] UX Researcher: Open a `Recruiting request` issue. Assign it to the relevant Research Coordinator.
* [ ] Research Coordinator: Recruit and schedule participants.
* [ ] Product Manager: Invite the UX Research calendar and any other interested parties to the interviews.
* [ ] Product Manager: Conduct the interviews.
* [ ] UX Researcher: Update the `Recruiting request` issue.
* [ ] Research Coordinator: Pay participants.
* [ ] Product Manager and UX Researcher: Synthesize the data and identify trends in Dovetail, resulting in insights.
* [ ] UX Researcher: Update the `Problem validation` research issue. Link to findings in Dovetail. Unmark as `confidential` if applicable. Close issue.
```

##### Surveys
```
* [ ] Product Manager: Draft the survey.
* [ ] UX Researcher: Transfer the survey questions to Qualtrics.
* [ ] UX Researcher: Open a `Recruiting request` issue. Assign it to the relevant Research Coordinator.
* [ ] Research Coordinator: Distribute the survey to a sample of participants.
* [ ] UX Researcher: Review responses received so far. Amend survey if necessary. Advise Research Coordinator to continue recruitment.
* [ ] UX Researcher: Notify Research Coordinator of survey closure.
* [ ] UX Researcher: Update the `Recruiting request` issue.
* [ ] Research Coordinator: Pay participants.
* [ ] Product Manager and UX Researcher: Synthesize the data and identify trends, resulting in insights.
* [ ] UX Researcher: Document insights in Dovetail
* [ ] UX Researcher: Update the `Problem validation` research issue. Link to findings in Dovetail. Unmark as `confidential` if applicable. Close issue.
```

##### Usability Testing
````
* [ ] Product Designer: Create a prototype.
* [ ] Product Designer: Create the screening survey in Qualtrics.
* [ ] Product Designer: Open a `Recruiting request` issue. Assign it to the relevant Research Coordinator.
* [ ] Product Designer: Draft the usability testing script.
* [ ] Product Design Manager: Review the usability testing script and provide feedback.
* [ ] Product Designer: Invite the UX Research calendar and any other interested parties to the usability testing sessions.
* [ ] Product Designer: Conduct one usability testing session. Amend script if necessary.
* [ ] Product Designer: Conduct remaining usability testing sessions.
* [ ] Product Designer: Update the `Recruiting request` issue.
* [ ] Research Coordinator: Pay users.
* [ ] Product Manager and Product Designer: Synthesize the data and identify trends in Dovetail, resulting in insights.
* [ ] Product Design Manager: Review insights and provide feedback, if needed.
* [ ] Product Designer: Update the `Solution validation` research issue. Link to findings in Dovetail. Unmark as `confidential` if applicable. Close issue.
````