---
layout: markdown_page
title: "Virtual DevOps Automation Workshop Presentations"
noindex: true
---

## Presentations

Thank you for joining us at a Virtual DevOps Automation Workshop! Below are the presentations from the workshops for you to reference.

* [Intro to GitLab](https://about.gitlab.com/resources/downloads/presentation-VirtualWorkshop-Introduction-to-GitLab.pdf)
* [GitLab as a Project Management Tool](https://about.gitlab.com/resources/downloads/presentation-VirtualWorkshop-GitLab-PM-Tool.pdf)
* [GitLab Basics for Developers](https://about.gitlab.com/resources/downloads/presentation-VirtualWorkshop-Gitlab-Basics-Developers.pdf)
* [Digging Deepers into CI/CD](https://about.gitlab.com/resources/downloads/presentation-VirtualWorkshop-CI-CD-Workshop.pdf)
