---
layout: handbook-page-toc
title: "Release Day Duties"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Every 22nd of the month we release a new version of GitLab. More often than not we get a spike in community mentions. To help deal with this we have dedicated release advocates that own the effort of responding to community mentions on/after a release.

Every month a different advocate has release advocate duty, which rotates on a monthly basis. If the release day takes place on a weekend, one of the advocates is assigned to monitor the traffic and to process mentions. We keep track of the assignments on the `Community Advocates` GitLab team calendar.

The two channels that we see the biggest increases in are:

* [The GitLab blog](/blog/)
* [HackerNews](https://news.ycombinator.com/news)

### Release day tasks

- Copy the overview of the main features or improvements from the beginning of the release blog post and post it to the HackerNews story that comes out during the release. You can use the [11.8 summary post](https://news.ycombinator.com/item?id=19228781) as an example.
- Consider engaging with an [Advocate-for-a-day](#advocate-for-a-day) in advance.
- Monitor the `#release-post` Slack channel throughout the day to be ready at the time the release blog post is published.
- If you are on release duty and will be offline for a period of time, let another advocate know so they can monitor.