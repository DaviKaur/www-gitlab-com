---
layout: handbook-page-toc
title: Communication tips
category: General
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Overview

This page is intended to provide general communication tips for GitLab Support
Department. Anything related to communication useful for this department is
welcome here. It is meant to be the extension of [GitLab Communication handbook page](https://about.gitlab.com/handbook/communication/).

*Examples:*
 1. *How can Support Engineer efficiently talk to other Support Engineers via Slack, Zoom or e-mail?*
 2. *How can Support Engineer get most of their interaction with Technical Account Manager?*

---

##### Slack reactions

In addition to `:white_check_mark:` emoji that is mentioned in [General Guidelines for Communication at GitLab](https://about.gitlab.com/handbook/communication/#general-guidelines), consider utilizing the following emojis within Support Slack channels as well:

1. `:idontknow:` - when another Support Engineer is asking a question which you don't know the answer to
1. `:red_circle:` - when another Support Engineer is asking for others to take a look at something or similar, but you are unable to do that within reasonable time (Example is when another engineer is asking for someone to replace them for their on-call shift, but you are unavailable at the time they asked for. Another example is when [SLAH](https://about.gitlab.com/handbook/support/workflows/meeting-service-level-objectives.html#what-is-the-sla-hawk-role) is asking whether someone can respond to ticket breaching in one hour, but you have no bandwidth to do that within the SLA.)

Note that it is better to send some signal (even though it might mean you cannot help) than to ignore the message that has been sent. This helps the person who started the thread as it avoids the situation where they still hope someone will chime in and makes them focus on searching for help elsewhere. Of course, whenever you can, share the idea you have in your mind no matter how trivial you consider it to be, as you never know what can lead someone else in the right direction.

##### Tickets

Whenever there is a discussion in Slack regarding a ticket, please remember to 
add a link to the discussion as an internal note on the ticket. This will allow 
anyone working on the ticket after you to easily find the discussion.

