---
layout: handbook-page-toc
title: "Continuous Delivery (CD) / Release Stage Playbook"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [Customer Success homepage](/handbook/customer-success/) for additional Customer Success handbook content.

---

### Procedure 

The following are the recommended steps to discover, position, lead value discussions, and drive adoption (TAM only).

1. 

### Discovery

The following are recommended questions for discovering customer needs.

### Positioning 

- Competitive Assessments
- Demo guides and recordings

### Adoption

- Product documentation (Content owner: Product and Engineering Teams)
- Training assets
- Paid services
- Adoption Map

| Feature / Use Case | F/C  | Basic | S/P  | G/U  |
| ------------------ | ---- | ----- | ---- | ---- |
|                    |      |       |      |      |
|                    |      |       |      |      |
|                    |      |       |      |      |

The table includes free/community and paid tiers associated with GitLab's self-managed and cloud offering.

- F/C = Free / Core
- Basic = Bronze/Starter 
- S/P = Silver / Premium
- G/U = Gold / Ultimate

### Reporting and Metrics

- Link to telemetry metrics

### Learning Resources 

- [Customer Use Case CD](/handbook/use-cases/#3-continuous-delivery-cd)
- CS Skills Exchange: CD Deep Dive: coming soon
